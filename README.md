# heap-usage

Aplicación gráfica para mostrar el uso interno de la memoria de Java.

## Compilar

Ejecutar `mvn jar:jar`.

## Uso

Ejecutar `java -jar heap-usage-1.0.0.jar`.