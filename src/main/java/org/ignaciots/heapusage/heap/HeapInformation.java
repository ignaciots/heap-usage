package org.ignaciots.heapusage.heap;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ignaciots.heapusage.unit.DigitalInformationUnit;

/**
 * Class that controls the heap data information
 * @author nacho
 * @version 1.0
 */

public class HeapInformation {
	
	private final DigitalInformationUnit unit;
	private final List<Object> heapData = new ArrayList<Object>(); //Array of heap data references
	private boolean isDebug; //debug flag
	
	
	/**
	 * Creates a new heap information control
	 * @param unit The digital unit that will provide the heap information control
	 */
	public HeapInformation(DigitalInformationUnit unit) {
		this.unit = unit;
		this.isDebug = false;
	}
	
	/**
	 * Creates a new heap information control
	 * @param unit The digital unit that will provide the heap information control
	 * @param isDebug The debug flag. When set the program will print debug data 
	 */
	public HeapInformation(DigitalInformationUnit unit, boolean isDebug) {
		this(unit);
		this.isDebug = isDebug;
	}
	
	/**
	 * Gets the used amount of memory, in a specific digital information unit
	 * @return The used amount of memory, in a specific digital information unit
	 */
	public int getUsedMemory() {
		long freeMemory = Runtime.getRuntime().freeMemory();
		long totalMemory = Runtime.getRuntime().totalMemory();
		long usedMemory = totalMemory - freeMemory;
		return this.unit.conversion(usedMemory);
	}
	
	/**
	 * Gets the total amount of memory, in a specific digital information unit
	 * @return The total amount of memory, in a specific digital information unit
	 */
	public int getTotalMemory() {
		return this.unit.conversion(Runtime.getRuntime().totalMemory());
	}
	
	/**
	 * Adds data to the heap
	 */
	public void addHeapItem() {
		//TODO Let user specify heap allocation size
		int numberOf64Blocks = 1 << 25; //This will result in 256 MiB item 
		heapData.add(new long[numberOf64Blocks]);
		printDebug(System.out, "Added %s mebibytes of data", ((64L * (numberOf64Blocks)) / 8L) / (1 << 20)); //Conversion of bytes to MiB
	}
	
	/**
	 * Clears the heap data
	 */
	public void clearData() {
		heapData.clear();
		printDebug(System.out, "Cleared data");
	}
	
	/**
	 * Runs the virtual machine garbage collector
	 */
	public void runGarbageColletor() {
		Runtime.getRuntime().gc();
		printDebug(System.out, "Run garbage collector");
	}
	
	private void printDebug(PrintStream out, String format, Object... args) {
		DateFormat dateFormat = DateFormat.getInstance();
		if (isDebug) out.println(String.format("[%s] %s", dateFormat.format(new Date()), String.format(format, args)));
	}

}
