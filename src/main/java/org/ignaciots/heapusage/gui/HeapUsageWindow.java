package org.ignaciots.heapusage.gui;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.BevelBorder;
import java.awt.FlowLayout;
import javax.swing.event.ChangeListener;

import org.ignaciots.heapusage.gui.status.HeapStatusBar;
import org.ignaciots.heapusage.gui.status.HeapStatusBarUpdater;
import org.ignaciots.heapusage.heap.HeapInformation;
import org.ignaciots.heapusage.unit.MebibyteInformationUnit;

import javax.swing.event.ChangeEvent;
import java.awt.Color;
import java.awt.Font;

/**
 * Window of the main program
 * @author nacho
 * @version 1.0
 */

public class HeapUsageWindow extends JFrame {

	private static final long serialVersionUID = -6551338576765215720L;
	
	private JPanel contentPane;
	private JPanel mainPanel;
	private JPanel progressBarPanel;
	private HeapStatusBar heapProgressBar;
	private JPanel buttonPanel;
	private JButton btnAddItem;
	
	private HeapInformation heapInformation = new HeapInformation(new MebibyteInformationUnit(), true);
	private JButton btnRunGc;
	private JButton btnClearHeap;
	

	/**
	 * Create the frame.
	 */
	public HeapUsageWindow() {
		setTitle("Heap Status Bar");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getMainPanel(), BorderLayout.CENTER);
		new Thread(new HeapStatusBarUpdater(heapProgressBar, heapInformation)).start();;
	}

	private JPanel getMainPanel() {
		if (mainPanel == null) {
			mainPanel = new JPanel();
			mainPanel.setLayout(new BorderLayout(0, 0));
			mainPanel.add(getProgressBarPanel(), BorderLayout.CENTER);
			mainPanel.add(getButtonPanel(), BorderLayout.SOUTH);
		}
		return mainPanel;
	}
	
	private JPanel getProgressBarPanel() {
		if (progressBarPanel == null) {
			progressBarPanel = new JPanel();
			FlowLayout flowLayout = (FlowLayout) progressBarPanel.getLayout();
			flowLayout.setVgap(100);
			progressBarPanel.setBorder(null);
			progressBarPanel.add(getHeapProgressBar());
		}
		return progressBarPanel;
	}
	
	private HeapStatusBar getHeapProgressBar() {
		if (heapProgressBar == null) {
			heapProgressBar = new HeapStatusBar();
			heapProgressBar.addChangeListener(new HeapProgressBarChangeListener());
			heapProgressBar.setToolTipText("Heap status size");
			heapProgressBar.setStringPainted(true);
		}
		return heapProgressBar;
	}
	
	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel();
			buttonPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			buttonPanel.add(getBtnAddItem());
			buttonPanel.add(getBtnRunGc());
			buttonPanel.add(getBtnClearHeap());
		}
		return buttonPanel;
	}
	
	private JButton getBtnAddItem() {
		if (btnAddItem == null) {
			btnAddItem = new JButton("Add Item");
			btnAddItem.setFont(new Font("Serif", Font.BOLD, 14));
			btnAddItem.setBackground(new Color(204, 0, 51));
			btnAddItem.setMnemonic('A');
			btnAddItem.setToolTipText("Add data to the heap");
			btnAddItem.addActionListener(new BtnAddItemActionListener());
		}
		return btnAddItem;
	}
	
	private JButton getBtnRunGc() {
		if (btnRunGc == null) {
			btnRunGc = new JButton("Run GC");
			btnRunGc.addActionListener(new BtnRunGcActionListener());
			btnRunGc.setFont(new Font("Serif", Font.BOLD, 14));
			btnRunGc.setBackground(new Color(204, 0, 51));
			btnRunGc.setToolTipText("Runs the garbage collector");
			btnRunGc.setMnemonic('R');
		}
		return btnRunGc;
	}
	
	private JButton getBtnClearHeap() {
		if (btnClearHeap == null) {
			btnClearHeap = new JButton("Clear Heap");
			btnClearHeap.addActionListener(new BtnClearHeapActionListener());
			btnClearHeap.setFont(new Font("Serif", Font.BOLD, 14));
			btnClearHeap.setBackground(new Color(204, 0, 51));
			btnClearHeap.setMnemonic('C');
			btnClearHeap.setToolTipText("Clears the heap items");
		}
		return btnClearHeap;
	}
	
	private class BtnAddItemActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				heapInformation.addHeapItem();
			} catch (OutOfMemoryError e) {
				handleOutOfMemoryError();
			}
		}
	}
	
	private void handleOutOfMemoryError() {
		JOptionPane.showMessageDialog(this, "There is not enough heap memory to allocate a new item.", "Out of heap memory", JOptionPane.ERROR_MESSAGE);
	}
	
	private class HeapProgressBarChangeListener implements ChangeListener {
		@Override
		public void stateChanged(ChangeEvent arg0) {
			getHeapProgressBar().updateDisplayedString();
		}
	}
	
	private class BtnRunGcActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			heapInformation.runGarbageColletor();
		}
	}
	private class BtnClearHeapActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			heapInformation.clearData();
		}
	}
	
}
