package org.ignaciots.heapusage.gui.status;

import javax.swing.BoundedRangeModel;
import javax.swing.JProgressBar;

/**
 * Progress bar with custom string
 * @author nacho
 * @version 1.0
 */

public class HeapStatusBar extends JProgressBar {
	
	private static final long serialVersionUID = -5220805756339367085L;

	
	public HeapStatusBar() {
		super();
		updateDisplayedString();
	}

	public HeapStatusBar(int orient) {
		super(orient);
		updateDisplayedString();
	}

	public HeapStatusBar(BoundedRangeModel newModel) {
		super(newModel);
		updateDisplayedString();
	}

	public HeapStatusBar(int min, int max) {
		super(min, max);
		updateDisplayedString();
	}

	public HeapStatusBar(int orient, int min, int max) {
		super(orient, min, max);
		updateDisplayedString();
	}
	
	public void updateDisplayedString() {
		this.progressString = String.format("%dM of %dM", getValue(), getMaximum());
	}
	
}
