package org.ignaciots.heapusage.gui.status;

import javax.swing.JProgressBar;

import org.ignaciots.heapusage.heap.HeapInformation;

/**
 * Runnable class to update the heap status bar with the heap information 
 * @author nacho
 * @version 1.0
 */

public class HeapStatusBarUpdater implements Runnable {

    private JProgressBar heapStatusBar;
    private HeapInformation heapInformation;

    
    /**
     * Creates a runnable to update the heap status bar with the heap information
     * @param heapStatusBar The heap status bar object
     * @param heapInformation The heap information object
     */
    public HeapStatusBarUpdater(JProgressBar heapStatusBar, HeapInformation heapInformation) {
    	if (heapStatusBar == null) {
    		throw new IllegalArgumentException("Heap status bar must not be null");
    	}
    	if (heapInformation == null) {
    		throw new IllegalArgumentException("Heap information must not be null");
    	}
    	
        this.heapInformation = heapInformation;
        this.heapStatusBar = heapStatusBar;
    }

    /**
     * Action of the thread will be executed here. The value of the progress bar will be set here.
     */
    @Override
	public void run() {
        do {
            heapStatusBar.setMaximum(heapInformation.getTotalMemory());
            heapStatusBar.setValue(heapInformation.getUsedMemory());
            try {
                java.lang.Thread.sleep(500L);
            } catch (java.lang.InterruptedException ex) {
                ex.printStackTrace();
            }
        } while (true);
    }
}
