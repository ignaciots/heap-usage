package org.ignaciots.heapusage.main;

import java.awt.EventQueue;

import org.ignaciots.heapusage.gui.HeapUsageWindow;

/**
 * Program main class
 * @author nacho
 * @version 1.0
 */

public class HeapUsage {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					HeapUsageWindow frame = new HeapUsageWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
