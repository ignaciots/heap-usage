package org.ignaciots.heapusage.unit;

public class MebibyteInformationUnit implements DigitalInformationUnit {

	@Override
	public int conversion(long data) {
		return (int) (data / (1 << 20));
	}

}
