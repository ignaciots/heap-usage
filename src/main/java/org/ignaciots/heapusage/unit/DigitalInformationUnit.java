package org.ignaciots.heapusage.unit;

/**
 * Interface that represents a digital information unit 
 * (byte, mebibyte, kibibyte...)
 * @author nacho
 * @version 1.0
 */

public interface DigitalInformationUnit {
	
	/**
	 * Converts a byte data into a another digital information unit
	 * @param data The data in bytes
	 * @return The data in a specific digital information unit
	 */
	int conversion(long data);

}
